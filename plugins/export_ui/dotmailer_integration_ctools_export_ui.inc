<?php
/**
 * @file
 * Custom Help Text export_ui plugin.
 */

module_load_include('php', 'ctools', 'plugins/export_ui/ctools_export_ui.class');

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  // As defined in hook_schema().
  'schema' => 'dotmailer_integration',
  // Define a permission users must have to access these pages.
  'access' => 'manage dotmailer integration',
  // Define the menu item.
  'menu' => array(
    'menu item' => 'dotmailer',
    'menu prefix' => 'admin/config/services/dotmailer-integration',
    'menu title' => 'DotMailer Integration',
    'menu description' => 'DotMailer Integration.',
  ),
  // Define user interface texts.
  'title singular' => t('DotMailer Integration'),
  'title plural' => t('DotMailer Integrations'),
  'title singular proper' => t('DotMailer Integration'),
  'title plural proper' => t('DotMailer Integrations'),
  'handler' => 'dotmailer_integration_export_ui',
  'form' => array(
    'settings' => 'dotmailer_integration_edit_form',
    'submit' => 'dotmailer_integration_edit_form_submit',
    'validate' => 'dotmailer_integration_edit_form_validate'
    // 'submit' and 'validate' are also valid callbacks.
  ),
);

/**
 * @param $form
 * @param $form_state
 * @return mixed
 */
function dotmailer_integration_edit_form(&$form, &$form_state) {

  $dotmailer_account = $form_state['item'];

  $form['info']['name']['#machine_name']['exists'] = 'dotmailer_integration_machine_name_validate';
  $form['info']['name']['#machine_name']['replace_pattern'] = '[^a-z0-9_]+';
  $form['info']['name']['#machine_name']['replace'] = '_';

  $form['dotmailer_integration_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Set new username'),
    '#description' => t("Username for the DotMailer API."),
    '#required' => TRUE,
    '#default_value' => $dotmailer_account->username,
  );

  $form['dotmailer_integration_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Set new password'),
    '#description' => t("Password for the DotMailer API."),
    '#required' => TRUE,
    '#default_value' => $dotmailer_account->password,
  );

  $form['dotmailer_integration_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is Default?'),
    '#description' => t("Is this the default DotMailer account?"),
    '#required' => FALSE,
    '#default_value' => $dotmailer_account->isdefault,
  );

  $default_present_query = db_select('dotmailer_integration', 'd')
    ->fields('d', array('pid'))
    ->condition('isdefault', 1)
    ->range(0, 1)
    ->execute();

  $default_present_result = $default_present_query->fetchAssoc();

  if ($default_present_result == FALSE) {
    $form['dotmailer_integration_default']['#default_value'] = 1;
    $form['dotmailer_integration_default']['#disabled'] = TRUE;
  }

  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function dotmailer_integration_edit_form_validate(&$form, &$form_state) {
  form_validate_machine_name($form['info']['name'], $form_state);
}

/**
 * @param $value
 * @param $element
 * @param $form_state
 * @return bool
 */
function dotmailer_integration_machine_name_validate($value, $element, $form_state) {

  $count = db_select('dotmailer_integration', 'd')
    ->fields('d', array('name'))
    ->condition('name', $value)
    ->countQuery()
    ->execute()
    ->fetchField();

  return ($count > 0 ? TRUE : FALSE);
}

/**
 * @param $form
 * @param $form_state
 */
function dotmailer_integration_edit_form_submit(&$form, &$form_state) {
  $dotmailer_account = $form_state['item'];

  $dotmailer_account->username = $form_state['values']['dotmailer_integration_username'];
  $dotmailer_account->password = $form_state['values']['dotmailer_integration_password'];
  $dotmailer_account->isdefault = $form_state['values']['dotmailer_integration_default'];

  if ($dotmailer_account->isdefault == 1) {
    // Set all Dotmailer Integrations to be non-default before saving the new default.
    db_update('dotmailer_integration')
      ->fields(array('isdefault' => 0))
      ->execute();
  }
}

/**
 * Class for DotMailer export UI.
 */
class dotmailer_integration_export_ui extends ctools_export_ui {

  /**
   * Builds the operation links for a specific exportable item.
   */
  function build_operations($item) {
    $plugin = $this->plugin;
    $schema = ctools_export_get_schema($plugin['schema']);
    $operations = $plugin['allowed operations'];
    $operations['import'] = FALSE;

    if ($item->{$schema['export']['export type string']} == t('Normal')) {
      $operations['revert'] = FALSE;
    }
    elseif ($item->{$schema['export']['export type string']} == t('Overridden')) {
      $operations['delete'] = FALSE;
    }
    else {
      $operations['revert'] = FALSE;
      $operations['delete'] = FALSE;
    }

    // Disable the enable and disable commands.
    $operations['disable'] = FALSE;
    $operations['enable'] = FALSE;

    $allowed_operations = array();

    foreach ($operations as $op => $info) {
      if (!empty($info)) {
        $allowed_operations[$op] = array(
          'title' => $info['title'],
          'href' => ctools_export_ui_plugin_menu_path($plugin, $op, $item->{$this->plugin['export']['key']}),
        );
        if (!empty($info['ajax'])) {
          $allowed_operations[$op]['attributes'] = array('class' => array('use-ajax'));
        }
        if (!empty($info['token'])) {
          $allowed_operations[$op]['query'] = array('token' => drupal_get_token($op));
        }
      }
    }

    $allowed_operations['test_api'] = array(
      'title' => t('Test API'),
      'href' => 'admin/config/services/dotmailer-integration/dotmailer/list/' . $item->{$this->plugin['export']['key']} . '/test_api'
    );

    return $allowed_operations;
  }

  /**
   * Create the filter/sort form at the top of a list of exports.
   *
   * This handles the very default conditions, and most lists are expected
   * to override this and call through to parent::list_form() in order to
   * get the base form and then modify it as necessary to add search
   * gadgets for custom fields.
   */
  function list_form(&$form, &$form_state) {
    // This forces the form to *always* treat as submitted which is
    // necessary to make it work.
    $form['#token'] = FALSE;
    if (empty($form_state['input'])) {
      $form["#post"] = TRUE;
    }

    // Add the 'q' in if we are not using clean URLs or it can get lost when
    // using this kind of form.
    if (!variable_get('clean_url', FALSE)) {
      $form['q'] = array(
        '#type' => 'hidden',
        '#value' => $_GET['q'],
      );
    }

    $all = array('all' => t('- All -'));

    $form['top row'] = array(
      '#prefix' => '<div class="ctools-export-ui-row ctools-export-ui-top-row clearfix">',
      '#suffix' => '</div>',
    );

    $form['bottom row'] = array(
      '#prefix' => '<div class="ctools-export-ui-row ctools-export-ui-bottom-row clearfix">',
      '#suffix' => '</div>',
    );

    $form['top row']['storage'] = array(
      '#type' => 'select',
      '#title' => t('Storage'),
      '#options' => $all + array(
          t('Normal') => t('Normal'),
          t('Default') => t('Default'),
          t('Overridden') => t('Overridden'),
        ),
      '#default_value' => 'all',
    );

    $form['top row']['search'] = array(
      '#type' => 'textfield',
      '#title' => t('Search'),
    );

    $form['bottom row']['order'] = array(
      '#type' => 'select',
      '#title' => t('Sort by'),
      '#options' => $this->list_sort_options(),
      '#default_value' => 'disabled',
    );

    $form['bottom row']['sort'] = array(
      '#type' => 'select',
      '#title' => t('Order'),
      '#options' => array(
        'asc' => t('Up'),
        'desc' => t('Down'),
      ),
      '#default_value' => 'asc',
    );

    $form['bottom row']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'ctools-export-ui-list-items-apply',
      '#value' => t('Apply'),
      '#attributes' => array('class' => array('use-ajax-submit ctools-auto-submit-click')),
    );

    $form['bottom row']['reset'] = array(
      '#type' => 'submit',
      '#id' => 'ctools-export-ui-list-items-apply',
      '#value' => t('Reset'),
      '#attributes' => array('class' => array('use-ajax-submit')),
    );

    $form['#prefix'] = '<div class="clearfix">';
    $form['#suffix'] = '</div>';
    $form['#attached']['js'] = array(ctools_attach_js('auto-submit'));
    $form['#attached']['library'][] = array('system', 'drupal.ajax');
    $form['#attached']['library'][] = array('system', 'jquery.form');
    $form['#attributes'] = array('class' => array('ctools-auto-submit-full-form'));
  }

  /**
   * Provide a list of sort options.
   *
   * Override this if you wish to provide more or change how these work.
   * The actual handling of the sorting will happen in build_row().
   */
  function list_sort_options() {
    if (!empty($this->plugin['export']['admin_title'])) {
      $options = array(
        'disabled' => t('title'),
        $this->plugin['export']['admin_title'] => t('Title'),
      );
    }
    else {
      $options = array(
        'disabled' => t('name'),
      );
    }

    $options += array(
      'name' => t('Name'),
      'storage' => t('Storage')
    );

    return $options;
  }

  /**
   * Build a row based on the item.
   *
   * By default all of the rows are placed into a table by the render
   * method, so this is building up a row suitable for theme('table').
   * This doesn't have to be true if you override both.
   */
  function list_build_row($item, &$form_state, $operations) {
    // Set up sorting
    $name = $item->{$this->plugin['export']['key']};
    $schema = ctools_export_get_schema($this->plugin['schema']);

    // Note: $item->{$schema['export']['export type string']} should have already been set up by export.inc so
    // we can use it safely.
    switch ($form_state['values']['order']) {
      case 'disabled':
        $this->sorts[$name] = empty($item->disabled) . $name;
        break;
      case 'title':
        $this->sorts[$name] = $item->{$this->plugin['export']['admin_title']};
        break;
      case 'name':
        $this->sorts[$name] = $name;
        break;
      case 'isdefault':
        $this->sorts[$name] = $item->isdefault;
        break;
      case 'storage':
        $this->sorts[$name] = $item->{$schema['export']['export type string']} . $name;
        break;
    }

    $this->rows[$name]['data'] = array();
    $this->rows[$name]['class'] = !empty($item->disabled) ? array('ctools-export-ui-disabled') : array('ctools-export-ui-enabled');

    // If we have an admin title, make it the first row.
    if (!empty($this->plugin['export']['admin_title'])) {
      $this->rows[$name]['data'][] = array(
        'data' => check_plain($item->{$this->plugin['export']['admin_title']}),
        'class' => array('ctools-export-ui-title')
      );
    }
    $this->rows[$name]['data'][] = array(
      'data' => check_plain($name),
      'class' => array('ctools-export-ui-name')
    );
    $this->rows[$name]['data'][] = array(
      'data' => check_plain($item->{$schema['export']['export type string']}),
      'class' => array('ctools-export-ui-storage')
    );

    $this->rows[$name]['data'][] = array(
      'data' => ($item->isdefault == 1 ? 'Yes' : 'No'),
      'class' => array('ctools-export-ui-storage')
    );

    $ops = theme('links__ctools_dropbutton', array(
      'links' => $operations,
      'attributes' => array('class' => array('links', 'inline'))
    ));

    $this->rows[$name]['data'][] = array(
      'data' => $ops,
      'class' => array('ctools-export-ui-operations')
    );

    // Add an automatic mouseover of the description if one exists.
    if (!empty($this->plugin['export']['admin_description'])) {
      $this->rows[$name]['title'] = $item->{$this->plugin['export']['admin_description']};
    }
  }

  /**
   * Provide the table header.
   *
   * If you've added columns via list_build_row() but are still using a
   * table, override this method to set up the table header.
   */
  function list_table_header() {
    $header = array();
    if (!empty($this->plugin['export']['admin_title'])) {
      $header[] = array(
        'data' => t('Title'),
        'class' => array('ctools-export-ui-title')
      );
    }

    $header[] = array(
      'data' => t('Name'),
      'class' => array('ctools-export-ui-name')
    );
    $header[] = array(
      'data' => t('Storage'),
      'class' => array('ctools-export-ui-storage')
    );
    $header[] = array(
      'data' => t('Is Default?'),
      'class' => array('ctools-export-ui-default')
    );
    $header[] = array(
      'data' => t('Operations'),
      'class' => array('ctools-export-ui-operations')
    );

    return $header;
  }

  /**
   * Determine if a row should be filtered out.
   *
   * This handles the default filters for the export UI list form. If you
   * added additional filters in list_form() then this is where you should
   * handle them.
   *
   * @return
   *   TRUE if the item should be excluded.
   */
  function list_filter($form_state, $item) {
    $schema = ctools_export_get_schema($this->plugin['schema']);
    if ($form_state['values']['storage'] != 'all' && $form_state['values']['storage'] != $item->{$schema['export']['export type string']}) {
      return TRUE;
    }

    if ($form_state['values']['search']) {
      $search = strtolower($form_state['values']['search']);
      foreach ($this->list_search_fields() as $field) {
        if (strpos(strtolower($item->$field), $search) !== FALSE) {
          $hit = TRUE;
          break;
        }
      }
      if (empty($hit)) {
        return TRUE;
      }
    }
  }
}
